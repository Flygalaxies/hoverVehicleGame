// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HoverVehicleGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOVERVEHICLEGAME_API AHoverVehicleGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
